from django.conf import settings
from django.conf.urls import include
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin

from rest_framework import routers
from chat import views as chat_views

router = routers.DefaultRouter()
router.register(r'rooms', chat_views.RoomViewSet, base_name='room')
router.register(r'messages', chat_views.MessageViewSet, base_name='message')


urlpatterns = [
    # django urls
    url(r'^admin/', include(admin.site.urls)),

    # 3rd party packeges urls
    url(r'^auth/', include('djoser.urls.authtoken')),

    # my urls
    url(r'^chat/', include(router.urls)),
    url(r'^chat/rooms/(?P<pk>[^/.]+)/last_messages/(?P<timestamp>[0-9]+)/$',
        chat_views.last_messages)

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
