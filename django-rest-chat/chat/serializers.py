from django.contrib.auth.models import User
from ember_drf.serializers import SideloadSerializer
from rest_framework import serializers

from chat.models import ConnectedRoomUser
from chat.models import Message
from chat.models import Room


class ConnectedUserSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.username')

    class Meta(object):
        model = ConnectedRoomUser
        fields = ('connected', 'user')


class MessageSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.username')

    class Meta(object):
        model = Message
        fields = ('id', 'url', 'created', 'user', 'type', 'text')


class MessageSideSerializer(SideloadSerializer, MessageSerializer):
    class Meta:
        base_serializer = MessageSerializer


class RoomBaseSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='user.username')

    class Meta(object):
        model = Room
        fields = ('id', 'created', 'url', 'name', 'owner',
                  'is_hidden', 'is_closed', 'is_private', 'is_public')


class RoomBaseDetailSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='user.username')
    # messages = MessageSerializer(source='message_set',
    #                              many=True, read_only=True)
    messages = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    connected_users = ConnectedUserSerializer('connected_users',
                                              many=True, read_only=True)

    class Meta(object):
        model = Room
        fields = ('id', 'created', 'url', 'name', 'owner',
                  'is_hidden', 'is_closed', 'is_private', 'is_public',
                  'messages', 'connected_users')


class RoomSideSerializer(SideloadSerializer, RoomBaseSerializer):
    class Meta(object):
        model = Room
        base_serializer = RoomBaseSerializer


class RoomListSerializer(RoomSideSerializer, RoomBaseSerializer):
    pass

class RoomDetailSerializer(RoomSideSerializer):

    class Meta(object):
        base_serializer = RoomBaseDetailSerializer


class RoomDetailNoAccessSerializer(RoomSideSerializer):
    pass


class PasswordSerializer(serializers.Serializer):
    password = serializers.CharField(style={'input_type': 'password'})


class MessageTextSerializer(serializers.Serializer):
    text = serializers.CharField()


class RoomCreateSerializer(serializers.Serializer):
    owner = serializers.CharField()
    name = serializers.CharField()
    password = serializers.CharField(allow_null=True)
    is_hidden = serializers.BooleanField(default=False)

    def is_valid(self, **kwargs):
        super(RoomCreateSerializer, self).is_valid(**kwargs)
        username = self.initial_data['owner']
        user_exists = User.objects.filter(username=username).exists()

        if not user_exists:
            raise

        return True

    def create(self, validated_data):
        username = validated_data['owner']

        user = User.objects.get(username=username)
        validated_data['user'] = user
        del validated_data['owner']

        return Room(**validated_data)

