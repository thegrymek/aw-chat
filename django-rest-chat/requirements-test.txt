-e .
unittest2
coverage==4.0.3
django-cors-headers==1.1.0
djoser==0.4.0
flake8==2.2.4
pep8==1.5.7
pyflakes==0.8.1
tox==2.1.1
