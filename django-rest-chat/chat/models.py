import datetime

from django.db import models
from django.db.models import signals
from django.utils.translation import ugettext_lazy as _

from chat import managers as chat_managers
from core import models as core_models


ACTIONS = (
    ('Connect', 'Connected'),
    ('Disconnect', 'Disconnected'),
    ('Message', 'Message')
)


class Room(core_models.TimeStamped, core_models.Ownable):
    """Chat room model.

    Only owner and admin can edit settings of room.
    """

    name = models.CharField(_('Room Name'), max_length=24,
                            db_index=True, unique=True)
    password = models.CharField(_('Password for room'), max_length=32,
                                blank=True, null=True)
    is_hidden = models.BooleanField(_('Is Hidden'), default=False)
    is_closed = models.BooleanField(_('Is Closed'), default=False)

    objects = models.Manager()
    public = chat_managers.PublicRoomManager()
    hidden = chat_managers.HiddenRoomsManager()

    class Meta(object):
        app_label = 'chat'
        ordering = ['-created', 'name']

    def __str__(self):
        return self.name

    @property
    def is_private(self):
        """Private rooms always with password."""
        return bool(self.password)

    def check_password(self, password):
        return self.password == password

    @property
    def is_public(self):
        return not (self.is_hidden or self.is_private or self.is_closed)

    def connect(self, user):
        connection, _ = ConnectedRoomUser.connections.get_or_create(room=self,
                                                                    user=user)

    def disconnect(self, user):
        connections = ConnectedRoomUser.connections.filter(room=self,
                                                           user=user)
        if not connections:
            raise AttributeError

        connection = connections[0]
        connection.disconnect()

    def is_user_connected(self, user):
        if user.is_anonymous():
            return False
        return ConnectedRoomUser.connections.filter(room=self,
                                                    user=user).count()

    def delete(self, using=None, keep_parents=False):
        self.is_closed = True
        self.save()

    def connected_users(self):
        return ConnectedRoomUser.connections.filter(room=self)

    def last_messages(self, timestamp=None):
        messages = Message.objects.filter(room=self).order_by('-created')
        if not timestamp:
            return messages[:5]

        now = datetime.datetime.fromtimestamp(float(timestamp))
        return list(messages.filter(created__gt=now))


class Message(core_models.TimeStamped, core_models.Ownable):
    """Message model."""

    room = models.ForeignKey(Room, db_index=True, related_name='messages')
    type = models.CharField(_('Type of action'), max_length=18,
                            choices=ACTIONS, null=True, blank=True)
    text = models.CharField(_('Text of message'), max_length=120)

    class Meta(object):
        app_label = 'chat'
        ordering = ['created', 'room']
        index_together = ['room', 'created']


class ConnectedRoomUser(core_models.Ownable):
    """Current users connected to room."""
    room = models.ForeignKey(Room, db_index=True)
    connected = models.DateTimeField(editable=False, auto_now_add=True)
    disconnected = models.DateTimeField(null=True, blank=True)

    objects = models.Manager()
    connections = chat_managers.ConnectedUsersManager()

    def disconnect(self):
        self.disconnected = datetime.datetime.now()
        self.save()

    @staticmethod
    def post_save(sender, instance, created, **kwargs):
        message = Message(room=instance.room, user=instance.user)
        if instance.disconnected:
            message.type = 'Disconnected'
            message.text = 'User %s disconnected' % instance.user
        else:
            message.type = 'Connected'
            message.text = 'User %s connected' % instance.user
        message.save()

signals.post_save.connect(ConnectedRoomUser.post_save, sender=ConnectedRoomUser)
