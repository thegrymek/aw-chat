from django.db import models
from django.utils.translation import ugettext_lazy as _


class Ownable(models.Model):
    """Abstract model that provides ownership of an object for a user."""

    user = models.ForeignKey("auth.User",
                             verbose_name=_("Author"),
                             related_name="%(class)ss")

    class Meta(object):
        abstract = True

    def is_editable(self, request):
        """Restrict in-line editing to the object's owner and superusers."""
        return request.user.is_superuser or request.user.id == self.user_id


class TimeStamped(models.Model):
    """Provides created and updated timestamps on models."""

    class Meta(object):
        abstract = True

    created = models.DateTimeField(editable=False, auto_now_add=True)
    updated = models.DateTimeField(editable=False, auto_now=True)
