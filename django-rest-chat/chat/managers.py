from django.db import models


class PublicRoomManager(models.Manager):
    def get_queryset(self):
        queryset = super(PublicRoomManager, self).get_queryset()
        return queryset.filter(is_hidden=False, is_closed=False)


class HiddenRoomsManager(models.Manager):
    def get_queryset(self):
        queryset = super(HiddenRoomsManager, self).get_queryset()
        return queryset.filter(is_closed=False)


class ConnectedUsersManager(models.Manager):
    def get_queryset(self):
        queryset = super(ConnectedUsersManager, self).get_queryset()
        return queryset.filter(disconnected__isnull=True)
