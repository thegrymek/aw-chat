
## DjangoRestFramework Chat

Its' a small project prepared for study exams. 

It uses djangorestframework for serialize, validate and render
json and djoser for ready-to-use token authorization and registration and ember for easy 
connect project with ember clients.


### Requirements

Packages needed for run server:

> Django 1.9
>
> django-cors-headers 1.1.0
>
> djangorestframework 3.2.4
>
> djoser 0.4.0
>
> ember-drf 1.19
>

Packages needed for run tests and checks:

> django 1.9
>
> django-cors-headers 1.1.0
>
> djangorestframework 3.3.1
>
> djoser 0.4.0
>
> ember-drf 1.19
>
> coverage 4.0.3
>
> flake8 2.2.4
>
> pep8 1.5.7
>
> pyflakes 0.8.1
>
> tox 2.1.1


### Installation

```
$ git clone https://thegrymek@bitbucket.org/thegrymek/aw-chat.git
$ cd aw-chat/django-rest-chat
$ mkvirtualenv aw-chat 
$ pip install -r requirements.txt
```


If you want to run tests do also

`$ pip install -r requirements-test.txt`


To run server type

```
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py runserver
```


To install example users and rooms

```
$ python manage.py loaddata user.json
$ python manage.py loaddata room.json
```


### Tests

Be sure you installed test requirements

`$ pip install -r requirements-test.txt`


To run code validator type

`$ flake8 .`


To run tox environment validator type

` $ tox . --skip-missing-interpreters`


To run tests type

`$ python manage.py test`


### How to

Run serer on localhost.

```
$ python manage.py runserver
```

and install example users and rooms

```
$ python manage.py loaddata user.json
$ python manage.py loaddata room.json
```

### Authentication - available services

**Register new user**

```
curl -X POST http://localhost:8000/auth/register/ \
     -d "username=user1&password=password123"
```


**Login with token**

```
curl -X POST http://localhost:8000/auth/login/ \
     -d "username=user1&password=password123"
```

token will be in response. Use that token in every next request.

```{"auth_token":"be400f08138dc52d789d735ce746506dfcd17921"}```


**Logout**

```
curl -X POST http://localhost:8000/auth/logout/ \
     -H 'Authorization: Token be400f08138dc52d789d735ce746506dfcd17921'
```


### Chat Services

Available services:

```
# [GET] get list of rooms
curl http://localhost:8000/chat/rooms/

# [GET] get detailed info about room
/chat/rooms/%id%/

# [POST][TOKEN_REQUIRED] create new room
# params:
#   name (required): name of the room
#   is_hidden: True or False
#   password: set password on room
/chat/rooms/%id%/

# [DELETE][TOKEN_REQUIRED] close the room
# Only owner can close the room or admin.
/chat/rooms/%id%/

# [POST][TOKEN_REQUIRED] connect to room
# params:
#   password (optional): only for private rooms
/chat/rooms/%id%/connect/

# [POST][TOKEN_REQUIRED] disconnect to room
/chat/rooms/%id%/disconnect/

# [POST][TOKEN_REQUIRED] send message to room
# params:
#     text (required): text message 
/chat/rooms/%id%/message/

# [GET] get last messages
# timestamp - datetime in number
/chat/rooms/%id%/last_messages/%timestamp%/

```

**Get list of rooms**

```
curl -H 'Accept: application/json; indent=4' http://localhost:8000/chat/rooms/
[
    {
        "created": "2015-12-27T12:47:47.126000Z",
        "url": "http://localhost:8000/chat/rooms/3/",
        "name": "Protected Room",
        "owner": "user1",
        "is_hidden": false,
        "is_closed": false,
        "is_private": true,
        "is_public": false
    },
    {
        "created": "2015-12-27T12:47:33.875000Z",
        "url": "http://localhost:8000/chat/rooms/2/",
        "name": "Public Room 2",
        "owner": "user2",
        "is_hidden": false,
        "is_closed": false,
        "is_private": false,
        "is_public": true
    },
    {
        "created": "2015-12-27T12:47:23.722000Z",
        "url": "http://localhost:8000/chat/rooms/1/",
        "name": "Public Room 1",
        "owner": "user1",
        "is_hidden": false,
        "is_closed": false,
        "is_private": false,
        "is_public": true
    }
]
```

Object room provides url in key 'url' to get into detailed info about room.

```"url": "http://localhost:8000/chat/rooms/3/",```

```
    "is_hidden": false,  # room os visible only for users with the link
    "is_closed": false,  # cannot connect or send messagage to this room
    "is_private": true,  # connect only by sending password
    "is_public": false   # access to room without any restrictions 
```


**Get detailed info about room**

```
curl -H 'Accept: application/json; indent=4' http:// localhost:8000/chat/rooms/1/
{
    "created": "2015-12-27T12:47:23.722000Z",
    "name": "Public Room 1",
    "owner": "user1",
    "is_hidden": false,
    "is_closed": false,
    "is_private": false,
    "is_public": true,
    "messages": [],
    "connected_users": []
}
```

Messages - ordered list of all messages from the past.

Connected users - list of currently connected users.

```
curl -H 'Accept: application/json; indent=4' http:// localhost:8000/chat/rooms/3/
{
    "created": "2015-12-27T12:47:47.126000Z",
    "url": "http://localhost:8000/chat/rooms/3/",
    "name": "Protected Room",
    "owner": "user1",
    "is_hidden": false,
    "is_closed": false,
    "is_private": true,
    "is_public": false
}
```

Keys messages and list of connected users are provided after password authorization to room.


**Connect and disconnect from the room**

**To join to room you have to be loged in.**


```
# getting token
curl -X POST -d "username=user1&password=password123" http://localhost:8000/auth/login/
{"auth_token":"be400f08138dc52d789d735ce746506dfcd17921"}

# connecting to room
curl -X POST http://localhost:8000/chat/rooms/1/connect/ \
     -H 'Accept: application/json; indent=4' \
     -H 'Authorization: Token be400f08138dc52d789d735ce746506dfcd17921' 
"OK"

# check status of room
curl -H 'Accept: application/json; indent=4' http://localhost:8000/chat/rooms/1/
{
    "created": "2015-12-27T12:47:23.722000Z",
    "name": "Public Room 1",
    "owner": "user1",
    "is_hidden": false,
    "is_closed": false,
    "is_private": false,
    "is_public": true,
    "messages": [
        {
            "created": "2016-01-02T12:37:52.766124Z",
            "user": "user1",
            "type": "Connected",
            "text": "User user1 connected"
        }
    ],
    "connected_users": [
        {
            "connected": "2016-01-02T12:37:52.764741Z",
            "user": "user1"
        }
    ]
}

# disconnecting from room
curl -X POST http://localhost:8000/chat/rooms/1/disconnect/ \
     -H 'Accept: application/json; indent=4' \
     -H 'Authorization: Token be400f08138dc52d789d735ce746506dfcd17921' 
"OK"

# again check status of room
curl -H 'Accept: application/json; indent=4' localhost:8000/chat/rooms/1/
{
    "created": "2015-12-27T12:47:23.722000Z",
    "name": "Public Room 1",
    "owner": "user1",
    "is_hidden": false,
    "is_closed": false,
    "is_private": false,
    "is_public": true,
    "messages": [
        {
            "created": "2016-01-02T12:37:52.766124Z",
            "user": "user1",
            "type": "Connected",
            "text": "User user1 connected"
        },
        {
            "created": "2016-01-02T12:40:48.803149Z",
            "user": "user1",
            "type": "Disconnected",
            "text": "User user1 disconnected"
        }
    ],
    "connected_users": []
}
```


**Connecting to room with password**


```
# getting token
curl -X POST -d "username=user1&password=password123"  localhost:8000/auth/login/
{"auth_token":"be400f08138dc52d789d735ce746506dfcd17921"}

# connecting to room, but wrong password
curl -X POST http://localhost:8000/chat/rooms/3/connect/ \
     -H 'Accept: application/json; indent=4' \
     -H 'Authorization: Token be400f08138dc52d789d735ce746506dfcd17921' 
{
    "error": "Wrong Password"
}

# connecting to room with valid password
curl -X POST http://localhost:8000/chat/rooms/3/connect/ \
     -d 'password=password123' \
     -H 'Accept: application/json; indent=4' \
     -H 'Authorization: Token be400f08138dc52d789d735ce746506dfcd17921' 
"OK"

# getting status of the room
curl localhost:8000/chat/rooms/3/ \
     -H 'Accept: application/json; indent=4' \
     -H 'Authorization: Token be400f08138dc52d789d735ce746506dfcd17921' 
{
    "created": "2015-12-27T12:47:47.126000Z",
    "name": "Protected Room",
    "owner": "user1",
    "is_hidden": false,
    "is_closed": false,
    "is_private": true,
    "is_public": false,
    "messages": [
        {
            "created": "2016-01-02T12:52:30.807169Z",
            "user": "user1",
            "type": "Connected",
            "text": "User user1 connected"
        }
    ],
    "connected_users": [
        {
            "connected": "2016-01-02T12:59:16.612847Z",
            "user": "user1"
        }
    ]
}

# disconnecting from room, can be without password
curl -X POST http://localhost:8000/chat/rooms/3/disconnect/ \
     -H 'Accept: application/json; indent=4' \
     -H 'Authorization: Token be400f08138dc52d789d735ce746506dfcd17921' 
"OK"

# again getting status of the room, but you have to be in room.
curl localhost:8000/chat/rooms/3/ \
     -H 'Accept: application/json; indent=4' \
     -H 'Authorization: Token be400f08138dc52d789d735ce746506dfcd17921' 
{
    "created": "2015-12-27T12:47:47.126000Z",
    "url": "http://localhost:8000/chat/rooms/3/",
    "name": "Protected Room",
    "owner": "user1",
    "is_hidden": false,
    "is_closed": false,
    "is_private": true,
    "is_public": false
}
```


**Sending and watching room for messages.**

Warn, you have to be logged to send messages.

```
# getting token
curl -X POST -d "username=user1&password=password123" localhost:8000/auth/login/
{"auth_token":"be400f08138dc52d789d735ce746506dfcd17921"}

# connecting to room
curl -X POST http://localhost:8000/chat/rooms/1/connect/ \
     -H 'Accept: application/json; indent=4' \
     -H 'Authorization: Token be400f08138dc52d789d735ce746506dfcd17921' 
"OK"

# sending message
curl -X POST http://localhost:8000/chat/rooms/1/message/ \
     -d 'text=First' \
     -H 'Accept: application/json; indent=4' \
     -H 'Authorization: Token be400f08138dc52d789d735ce746506dfcd17921' 
"OK"

# getting status
curl localhost:8000/chat/rooms/1/ \
     -H 'Accept: application/json; indent=4'
{
    "created": "2015-12-27T12:47:23.722000Z",
    "name": "Public Room 1",
    "owner": "user1",
    "is_hidden": false,
    "is_closed": false,
    "is_private": false,
    "is_public": true,
    "messages": [
        {
            "created": "2016-01-02T12:37:52.766124Z",
            "user": "user1",
            "type": "Connected",
            "text": "User user1 connected"
        },
        {
            "created": "2016-01-02T12:40:48.803149Z",
            "user": "user1",
            "type": "Disconnected",
            "text": "User user1 disconnected"
        },
        {
            "created": "2016-01-02T13:36:20.045850Z",
            "user": "user1",
            "type": "Connected",
            "text": "User user1 connected"
        },
        {
            "created": "2016-01-02T13:37:01.825564Z",
            "user": "user1",
            "type": "Message",
            "text": "First"
        }
    ],
    "connected_users": [
        {
            "connected": "2016-01-02T13:36:20.045253Z",
            "user": "user1"
        }
    ]
}

# to watch changes constantly use timestamped service last_messages
# I use timestamp defined in number. Convert datetime to number. That's all.
curl localhost:8000/chat/rooms/1/last_messages/1/ \
     -H 'Accept: application/json; indent=4'
[
    {
        "created": "2016-01-02T13:37:01.825564Z",
        "user": "user1",
        "type": "Message",
        "text": "First"
    },
    {
        "created": "2016-01-02T12:37:52.766124Z",
        "user": "user1",
        "type": "Connected",
        "text": "User user1 connected"
    }
]

# another message
curl -X POST http://localhost:8000/chat/rooms/1/message/ \
     -d 'text=Second' \
     -H 'Accept: application/json; indent=4' \
     -H 'Authorization: Token be400f08138dc52d789d735ce746506dfcd17921' 
"OK"

curl localhost:8000/chat/rooms/1/last_messages/1451742354/ \
     -H 'Accept: application/json; indent=4'
[
    {
        "created": "2016-01-02T13:46:40.721443Z",
        "user": "user1",
        "type": "Message",
        "text": "Second"
    }
]
```


**Create and close room.**

```
# getting token
curl -X POST -d "username=user1&password=password123" http://localhost:8000/auth/login/
{"auth_token":"be400f08138dc52d789d735ce746506dfcd17921"}

# creating public room
curl -X POST -d "name=MyRoom" http://localhost:8000/chat/rooms/ \
     -H 'Authorization: Token be400f08138dc52d789d735ce746506dfcd17921'

# creating room with password
curl -X POST -d "name=MyRoom&password=password123" http://localhost:8000/chat/rooms/ \
     -H 'Authorization: Token be400f08138dc52d789d735ce746506dfcd17921'

# creating hidden room
curl -X POST -d "name=MyRoom&is_hidden=1" http://localhost:8000/chat/rooms/ \
     -H 'Authorization: Token be400f08138dc52d789d735ce746506dfcd17921'

# closing room
curl -X DELETE http://localhost:8000/chat/rooms/5/
     -H 'Authorization: Token be400f08138dc52d789d735ce746506dfcd17921'
```


### License

The MIT License (MIT)

Copyright (c) 2013 Andrzej Grymkowski

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
