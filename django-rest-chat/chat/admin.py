from django.contrib import admin

# Register your models here.
from chat.models import ConnectedRoomUser
from chat.models import Message
from chat.models import Room

admin.site.register(Room)
admin.site.register(Message)
admin.site.register(ConnectedRoomUser)
