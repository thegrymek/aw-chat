from datetime import datetime

from django.contrib.auth.models import User
from django.test import TestCase

from rest_framework.test import APIClient

from chat.models import Room


client = APIClient()


class BaseTestCase(TestCase):
    fixtures = ['user.json', 'room.json']

    def get_auth_token(self, user):
        response = self.client.post('/auth/login/',
                                    {'username': user.username, 'password': 'password123'})
        return response.data['auth_token']


class AuthTestCase(BaseTestCase):

    def testTokenAuth(self):
        user = User.objects.get(username='user1')
        response = self.client.post('/auth/login/',
                                    {'username': user.username, 'password': 'password123'})

        self.assertTrue('auth_token' in response.data)

        auth_token = response.data['auth_token']
        self.client.post('/auth/logout/', HTTP_AUTHORIZATION='Token {}'.format(auth_token))

        # logging again and comparing if token is different
        response = self.client.post('/auth/login/',
                                    {'username': user.username, 'password': 'password123'})
        self.assertNotEqual(auth_token, response.data['auth_token'])


class RoomTestCase(BaseTestCase):

    test_user = User.objects.get(username='user1')

    def testListPublicRooms(self):
        """Accessing for info for list of public rooms.

        Public room does we have access to list them?
        """
        rooms = Room.public.all()
        self.assertTrue(len(rooms) > 0, 'Should be declared at least one room')

        json = self.client.get('/chat/rooms/').data['rooms']
        room_names = list(map(lambda c: c['name'], json))

        self.assertEqual(len(room_names), len(rooms),
                         'Count of public rooms and response with public rooms '
                         'should be the same')

    def testDetailedPublicRooms(self):
        """Detailed room info.

        Does we have access to get detailed data of room.
        """
        rooms = self.client.get('/chat/rooms/').data['rooms']

        public_room = list(filter(lambda room: room['is_public'], rooms))[0]
        public_room = self.client.get(public_room['url']).data['room']

        # we have read access to public room so we can read everything
        self.assertTrue('name' in public_room)
        self.assertTrue('messages' in public_room)
        self.assertTrue('connected_users' in public_room)

    def testPrivateRoom(self):
        """Access Private Room.

        Private rooms should be visible for everyone
        but access should have only after connect to room.
        """
        rooms = self.client.get('/chat/rooms/').data['rooms']
        private_room = list(filter(lambda room: room['is_private'], rooms))[0]
        private_room = self.client.get(private_room['url']).data['room']

        # we havent access to private room so we see only part of info
        self.assertTrue('name' in private_room)
        self.assertFalse('messages' in private_room)
        self.assertFalse('connected_users' in private_room)

        private_room = Room.objects.get(name=private_room['name'])

        # connecting to the room
        response = self.client.post('/chat/rooms/%s/connect/' % private_room.id)
        # no access, we have to login
        self.assertEqual(response.status_code, 401)

        # logging - obtaining token password
        user = User.objects.get(username='user1')
        response = self.client.post('/auth/login/',
                                    {'username': user.username,
                                     'password': 'password123'})
        auth_token = response.data['auth_token']

        # connecting to the room but wrong password
        response = self.client.post('/chat/rooms/%s/connect/' % private_room.id,
                                    HTTP_AUTHORIZATION='Token {}'.format(auth_token))
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.data['error'], 'Wrong Password')

        # connecting with password
        response = self.client.post('/chat/rooms/%s/connect/' % private_room.id,
                                    {'password': private_room.password},
                                    HTTP_AUTHORIZATION='Token {}'.format(auth_token))
        self.assertEqual(response.status_code, 200)

        # we should have access so lets check it
        private_room = self.client.get('/chat/rooms/%s/' % private_room.id,
                                       HTTP_AUTHORIZATION='Token {}'.format(auth_token))
        private_room = private_room.data['room']

        self.assertTrue('name' in private_room)
        self.assertTrue('messages' in private_room)
        self.assertTrue('connected_users' in private_room)

        # check that we are connected
        self.assertEqual(private_room['connected_users'][0]['user'], user.username)

    def testConnectDisconnect(self):
        """Connecting and disconnecting to check messages.

        Testing whether response contains info about connection and disconnection.
        """
        public_room = Room.objects.get(name='Public Room 1')
        token = self.get_auth_token(self.test_user)
        response = self.client.post('/chat/rooms/%s/connect/' % public_room.id,
                                    HTTP_AUTHORIZATION='Token {}'.format(token))
        self.assertEqual(response.status_code, 200)

        data = self.client.get('/chat/rooms/%s/' % public_room.id,
                               HTTP_AUTHORIZATION='Token {}'.format(token)).data['room']

        self.assertEqual(data['connected_users'][0]['user'], self.test_user.username)
        self.assertEqual(len(data['messages']), 1)
        self.assertEqual(data['messages'][0]['type'], 'Connected')
        self.assertEqual(data['messages'][0]['text'], 'User %s connected' % self.test_user.username)

        response = self.client.post('/chat/rooms/%s/disconnect/' % public_room.id,
                                    HTTP_AUTHORIZATION='Token {}'.format(token))
        self.assertEqual(response.status_code, 200)

        data = self.client.get('/chat/rooms/%s/' % public_room.id,
                               HTTP_AUTHORIZATION='Token {}'.format(token)).data['room']
        self.assertTrue('connected_users' in data)
        self.assertEqual(len(data['connected_users']), 0)
        self.assertEqual(len(data['messages']), 2)
        self.assertEqual(data['messages'][1]['type'], 'Disconnected')
        self.assertEqual(data['messages'][1]['text'], 'User %s disconnected' % self.test_user.username)

    def testSendMessage(self):
        public_room = Room.objects.get(name='Public Room 1')
        token = self.get_auth_token(self.test_user)
        token = 'Token {}'.format(token)
        url = '/chat/rooms/%s/' % public_room.id
        response = self.client.post(url + 'connect/', HTTP_AUTHORIZATION=token)
        self.assertEqual(response.status_code, 200)

        # first message is that user is connected
        self.assertEqual(public_room.message_set.count(), 1)

        timestamp = (datetime.now() - datetime(1970, 1, 1)).total_seconds()

        # adding come messages
        response = self.client.post(url + 'message/', {'text': 'first message'},
                                    HTTP_AUTHORIZATION=token)
        self.assertEqual(response.status_code, 200)
        response = self.client.post(url + 'message/', {'text': 'second message'},
                                    HTTP_AUTHORIZATION=token)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(public_room.last_messages(timestamp)), 2)

        # check we see it in response
        response = self.client.get(url, HTTP_AUTHORIZATION=token)
        room = response.data['room']
        self.assertEqual(len(room['messages']), 3)
        self.assertEqual(room['messages'][1]['type'], 'Message')
        self.assertEqual(room['messages'][1]['text'], 'first message')

    def testCreateRoom(self):
        token = self.get_auth_token(self.test_user)
        token = 'Token {}'.format(token)

        # creating public room
        response = self.client.post('/chat/rooms/',
                                    {'name': 'My room'},
                                    HTTP_AUTHORIZATION=token)
        self.assertEqual(response.status_code, 201)

        room = Room.objects.get(name='My room')
        self.assertTrue(room.is_public)

        # creating private room
        response = self.client.post('/chat/rooms/',
                                    {'name': 'My private room',
                                     'password': 'password123'},
                                    HTTP_AUTHORIZATION=token)
        self.assertEqual(response.status_code, 201)

        room = Room.objects.get(name='My private room')
        self.assertTrue(room.is_private)
        self.assertTrue(room.check_password('password123'))

        # creating hidden room
        response = self.client.post('/chat/rooms/',
                                    {'name': 'My hidden room',
                                     'is_hidden': True},
                                    HTTP_AUTHORIZATION=token)
        self.assertEqual(response.status_code, 201)

        room = Room.objects.get(name='My hidden room')
        self.assertTrue(room.is_hidden)

    def testDestroyRoom(self):
        token = self.get_auth_token(self.test_user)
        token = 'Token {}'.format(token)

        # creating public room
        response = self.client.post('/chat/rooms/',
                                    {'name': 'My room'},
                                    HTTP_AUTHORIZATION=token)
        self.assertEqual(response.status_code, 201)

        room = Room.objects.get(name='My room')
        self.assertTrue(room.is_public)

        # closing public room
        response = self.client.delete('/chat/rooms/%s/' % room.id,
                                      HTTP_AUTHORIZATION=token)
        self.assertEqual(response.status_code, 200)

        room = Room.objects.get(name='My room')
        self.assertTrue(room.is_closed)

        # attempt to close room from other user
        admin = User.objects.get(id=1)
        room = Room(name='Admin Room', user=admin)
        room.save()

        response = self.client.delete('/chat/rooms/%s/' % room.id,
                                      HTTP_AUTHORIZATION=token)
        self.assertEqual(response.status_code, 403)

        # another test - admin can close every room
        response = self.client.post('/chat/rooms/',
                                    {'name': 'My room 2'},
                                    HTTP_AUTHORIZATION=token)
        self.assertEqual(response.status_code, 201)

        admin_token = self.get_auth_token(admin)
        admin_token = 'Token {}'.format(admin_token)

        response = self.client.delete('/chat/rooms/%s/' % room.id,
                                      HTTP_AUTHORIZATION=admin_token)
        self.assertEqual(response.status_code, 200)
