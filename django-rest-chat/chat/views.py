from django.shortcuts import get_object_or_404

from rest_framework import permissions
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.decorators import detail_route
from rest_framework.response import Response

from chat import serializers as chat_serializers
from chat.models import Message
from chat.models import Room


class MessageViewSet(viewsets.ViewSet):

    serializer_class = chat_serializers.MessageSideSerializer

    def get_queryset(self):
        return Message.objects.all()

    def list(self, request):
        queryset = self.get_queryset()
        serializer = self.serializer_class(queryset, many=True, context={'request': request})
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = self.get_queryset()
        room = get_object_or_404(queryset, pk=pk)
        serializer = self.serializer_class(room, context={'request': request})

        return Response(serializer.data)


class RoomViewSet(viewsets.ViewSet):

    serializer_class = chat_serializers.RoomListSerializer

    def get_queryset(self):
        user = getattr(self.request, 'user', None)
        queryset = Room.public.all()

        if not user:
            return queryset

        queryset = user.is_staff and Room.objects.all() or \
                   user.is_authenticated() and Room.hidden.all() or \
                   user.is_anonymous() and Room.public.all()

        return queryset

    def get_serializer_class(self, room):
        user = getattr(self.request, 'user', None)
        serialize_class = chat_serializers.RoomDetailNoAccessSerializer

        if room.is_public:
            return chat_serializers.RoomDetailSerializer

        if (room.is_private or room.is_hidden) and room.is_user_connected(user):
            return chat_serializers.RoomDetailSerializer

        return serialize_class

    @detail_route(methods=['post'], permission_classes=[permissions.IsAuthenticated])
    def create(self, request):
        user = getattr(self.request, 'user', None)
        data = request.data.copy()
        data['owner'] = user.username
        serializer = chat_serializers.RoomCreateSerializer(data=data,
                                                           context={'request': request})

        if not serializer.is_valid():
            return Response('error', status=status.HTTP_400_BAD_REQUEST)
        name = serializer.data['name']
        if Room.objects.filter(name=name).exists():
            return Response('Room with this name exists',
                            status=status.HTTP_400_BAD_REQUEST)

        serializer.create(serializer.data).save()
        return Response('OK', status=status.HTTP_201_CREATED)

    @detail_route(methods=['delete'], permission_classes=[permissions.IsAuthenticated])
    def destroy(self, request, pk=None):
        queryset = self.get_queryset()
        room = get_object_or_404(queryset, pk=pk)

        if room.is_editable(request):
            room.delete()
            return Response('OK', status=status.HTTP_200_OK)

        return Response('You are not owner of the room',
                        status=status.HTTP_403_FORBIDDEN)

    def list(self, request):
        queryset = self.get_queryset()
        serializer = chat_serializers.RoomListSerializer(queryset, many=True,
                                                         context={'request': request})
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = self.get_queryset()
        room = get_object_or_404(queryset, pk=pk)

        serialize_class = self.get_serializer_class(room)
        serializer = serialize_class(room, context={'request': request})

        return Response(serializer.data)

    @detail_route(methods=['post'], permission_classes=[permissions.IsAuthenticated])
    def connect(self, request, pk=None):
        user = getattr(self.request, 'user')
        queryset = self.get_queryset()
        room = get_object_or_404(queryset, pk=pk)

        if room.is_user_connected(user):
            return Response('OK', status=status.HTTP_200_OK)

        if room.is_public:
            room.connect(user)
            return Response('OK', status=status.HTTP_200_OK)

        if room.is_private:
            serializer = chat_serializers.PasswordSerializer(data=request.data)

            if serializer.is_valid():
                password = serializer.data['password']
                if room.check_password(password):
                    room.connect(user)
                    return Response('OK', status=status.HTTP_200_OK)

        return Response({'error': 'Wrong Password'},
                        status=status.HTTP_401_UNAUTHORIZED)

    @detail_route(methods=['post'], permission_classes=[permissions.IsAuthenticated])
    def disconnect(self, request, pk=None):
        user = getattr(self.request, 'user')
        queryset = self.get_queryset()
        room = get_object_or_404(queryset, pk=pk)

        if room.is_user_connected(user):
            room.disconnect(user)
        return Response('OK', status=status.HTTP_200_OK)

    @detail_route(methods=['post'], permission_classes=[permissions.IsAuthenticated])
    def message(self, request, pk=None):
        user = getattr(self.request, 'user')
        queryset = self.get_queryset()
        room = get_object_or_404(queryset, pk=pk)
        serializer = chat_serializers.MessageTextSerializer(data=request.data)

        if not serializer.is_valid():
            return Response('json is not valid', status=status.HTTP_400_BAD_REQUEST)

        if not room.is_user_connected(user):
            return Response('User must be connected to room', status=status.HTTP_400_BAD_REQUEST)

        text = serializer.data['text']
        message = Message(room=room, user=user, type='Message', text=text)
        message.save()
        return Response('OK', status=status.HTTP_200_OK)


@api_view(['GET'])
def last_messages(request, pk, timestamp=None):
    queryset = Room.objects.all()
    room = get_object_or_404(queryset, pk=pk)

    if not room.is_public:
        user = getattr(request, 'user', None)
        if not user or not room.is_user_connected(user):
            return Response({'error': 'No access'},
                            status=status.HTTP_401_UNAUTHORIZED)

    messages = room.last_messages(timestamp)
    serializer = chat_serializers.MessageSerializer(messages, many=True,
                                                    context={'request': request})
    return Response(serializer.data)
