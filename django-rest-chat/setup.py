# -*- coding: utf-8 -*-

import os
from setuptools import setup


def get_packages(package):
    return [dirpath
            for dirpath, dirnames, filenames in os.walk(package)
            if os.path.exists(os.path.join(dirpath, '__init__.py'))]


def get_requirements(file_name):
    return [i.strip() for i in open(file_name).readlines()]


setup(
    name='DjangoRestChat',
    version='0.0.1',
    description="Simple Rest Chat in djagno.",
    long_description="Simple Rest Chat in djagno.",
    author='Andrzej Grymkowski',
    author_email='andrzej.grymkowski@gmail.com',
    url='https://bitbucket.org/thegrymek/aw-chat',
    license='MIT',
    packages=get_packages('chat'),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: Unix',
        'Operating System :: OS Independent',
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3.4",
    ],
    install_requires=[],
    include_package_data=True
)
